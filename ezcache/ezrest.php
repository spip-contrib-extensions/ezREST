<?php
/**
 * Ce fichier contient les fonctions de service nécessitées par le plugin Cache Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches gérés par REST Factory si les fonctions de collection des données
 * sont directement codés en PHP.
 *
 * Dans le cas où les données JSON sont créées via des squelettes SPIP, le cache est déjà géré par SPIP.
 *
 * @param string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *
 * @return array<string, mixed> Tableau de la configuration brute du plugin Taxonomie.
 */
function ezrest_cache_configurer(string $plugin) : array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache.
	return [
		'reponse' => [
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => true,
			'nom_obligatoire' => ['type_requete', 'collection'],
			'nom_facultatif'  => ['complement'],
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'codage'          => 'json',
			'separateur'      => '-',
			'conservation'    => 3600 * 24,
			'administration'  => true
		],
		'index' => [
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => false,
			'nom_prefixe'     => 'index',
			'nom_obligatoire' => [],
			'nom_facultatif'  => [],
			'extension'       => '.txt',
			'securisation'    => false,
			'serialisation'   => true,
			'codage'          => '',
			'separateur'      => '',
			'conservation'    => 0,
			'administration'  => false
		],
	];
}

/**
 * Effectue le chargement du formulaire de vidage des caches de type `reponse` pour le plugin REST Factory.
 * L'intérêt est de permette le rangement des caches par type de requête.
 *
 * @uses cache_repertorier()
 * @uses cache_lire()
 *
 * @param string               $plugin        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                            ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array<string, mixed> $valeurs       Tableau des valeurs du formulaire à compléter
 * @param array                $options       Tableau d'options qui peut être fourni par un plugin utilisateur uniquement si celui-ci fait appel
 *                                            au formulaire. La page cache_vider de Cache Factory n'utilise pas ce paramètre.
 *                                            Le tableau est passé à la fonction de service de chargement du formulaire uniquement.
 * @param array<string, mixed> $configuration Configuration complète des caches du plugin utilisateur lue à partir de la meta de stockage.
 *
 * @return array<string, mixed> Tableau des valeurs du formulaire complété par la structure propre au type de cache.
 *
 * @throws Exception
 */
function ezrest_reponse_cache_formulaire_charger(string $plugin, array $valeurs, array $options, array $configuration) : array {
	// On constitue la liste des types de requêtes admises pour regrouper les caches selon ce premier critère.
	$types_requete = ['index', 'collection', 'ressource'];

	// On récupère les caches et leur description pour donner un maximum d'explication sur le contenu.
	include_spip('inc/ezcache_cache');
	foreach ($types_requete as $_type) {
		// On récupère les caches du service
		$caches = cache_repertorier('ezrest', 'reponse', ['type_requete' => $_type]);

		// Présentation des paramètres pour les collections (filtres) ou pour la ressource (filtres et ressource).
		if ($_type != 'index') {
			foreach ($caches as $_cle => $_cache) {
				// On traite le complément de la ressource ou des filtres de la collection pour un affichage précis.
				// -- lecture de l'index
				$cache_index = [];
				$index = cache_lire('ezrest', 'index', $cache_index);

				// -- détermination du fichier cache
				$fichier_cache = basename(dirname($_cle)) . '/' . basename($_cle);

				// -- fournir le nom exact des paramètres
				if (isset($index[$fichier_cache]['filtres'])) {
					$filtres = '';
					foreach ($index[$fichier_cache]['filtres'] as $_critere => $_valeur) {
						if ($filtres) {
							$filtres .= ' | ';
						}
						$filtres .= "{$_critere}={$_valeur}";
					}
					$caches[$_cle]['filtre'] = $filtres ?: $_cache['complement'];
				}
			}
		}

		// Si il existe des caches pour le service on stocke les informations recueillies
		if ($caches) {
			$valeurs['_caches']['reponse'][$_type]['titre'] = _T('ezrest:type_requete_' . $_type . '_titre');
			$valeurs['_caches']['reponse'][$_type]['liste'] = $caches;
		}
	}

	return $valeurs;
}

/**
 * Effectue le chargement du formulaire de vidage des caches de type `index` pour le plugin REST Factory.
 * L'index est unique et comme sa suppression peut être sensible, le formulaire affiche un avertissement.
 *
 * @uses cache_repertorier()
 *
 * @param string               $plugin        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                            ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array<string, mixed> $valeurs       Tableau des valeurs du formulaire à compléter
 * @param array                $options       Tableau d'options qui peut être fourni par un plugin utilisateur uniquement si celui-ci fait appel
 *                                            au formulaire. La page cache_vider de Cache Factory n'utilise pas ce paramètre.
 *                                            Le tableau est passé à la fonction de service de chargement du formulaire uniquement.
 * @param array<string, mixed> $configuration Configuration complète des caches du plugin utilisateur lue à partir de la meta de stockage.
 *
 * @return array<string, mixed> Tableau des valeurs du formulaire complété par la structure propre au type de cache.
 */
function ezrest_index_cache_formulaire_charger(string $plugin, array $valeurs, array $options, array $configuration) : array {
	// On récupère le cache index des caches réponses.
	include_spip('inc/ezcache_cache');
	$caches = cache_repertorier('ezrest', 'index');

	// On renvoie la structure attendue avec une explication
	if ($caches) {
		$valeurs['_caches']['index']['ezrest']['titre'] = '';
		$valeurs['_caches']['index']['ezrest']['liste'] = $caches;

		$valeurs['_explications']['index'] = _T('ezrest:form_vidage_index_explication');
	}

	return $valeurs;
}
