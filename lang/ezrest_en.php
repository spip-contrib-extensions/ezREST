<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ezrest-ezrest?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'collection_filtre_facultatif' => 'optional',
	'collection_filtre_fournisseur' => 'added by the "@module@" plugin.',
	'collection_filtre_obligatoire' => 'mandatory',
	'collection_ressource_non' => 'unauthorized reading',
	'collection_ressource_oui' => 'authorized reading, identifier "@ressource@"',

	// E
	'erreur_200_ok_message' => 'The data collected can be consulted in the "donnees" index.',
	'erreur_200_ok_titre' => 'The request has been processed successfully',
	'erreur_400_cle_obligatoire_nok_message' => 'Please use a valid "@valeur@" access key.',
	'erreur_400_cle_obligatoire_nok_titre' => 'The "@valeur@" access key is mandatory',
	'erreur_400_cle_valeur_format_nok_message' => 'Please use a value for the "@element@" access key that conforms to the "@extra@" format.',
	'erreur_400_cle_valeur_format_nok_titre' => 'The "@valeur@" value of the "@element@" access key is not valid',
	'erreur_400_cle_valeur_vide_nok_message' => 'Please use the access key "@valeur@" with a valid non-empty value.',
	'erreur_400_cle_valeur_vide_nok_titre' => 'Access key "@valeur@" cannot be empty',
	'erreur_400_collection_indisponible_message' => 'The API supports the use of the following collections: @extra@.',
	'erreur_400_collection_indisponible_titre' => 'The "@valeur@" collection is not provided by the API',
	'erreur_400_collection_nok_titre' => 'Problem with the "@valeur@" collection',
	'erreur_400_critere_nom_nok_message' => 'The "@collection@" collection supports the following parameters: @extra@.',
	'erreur_400_critere_nom_nok_titre' => 'The "@valeur@" parameter is not supported by the "@collection@" collection.',
	'erreur_400_critere_obligatoire_nok_message' => 'Please use the "@valeur@" parameter with an appropriate value.',
	'erreur_400_critere_obligatoire_nok_titre' => 'The "@valeur@" parameter is mandatory',
	'erreur_400_critere_valeur_format_nok_message' => 'Please use a value for the "@element@" parameter that conforms to the "@extra@" format.',
	'erreur_400_critere_valeur_format_nok_titre' => 'The "@valeur@" value of the "@element@" parameter is not valid',
	'erreur_400_critere_valeur_vide_nok_message' => 'Please use the "@valeur@" parameter with a non-empty allowed value.',
	'erreur_400_critere_valeur_vide_nok_titre' => 'The "@valeur@" parameter cannot be empty',
	'erreur_400_ressource_indisponible_message' => 'The API only provides resources for the following collections: @extra@.',
	'erreur_400_ressource_indisponible_titre' => 'The "@collection@" collection does not authorize access to a resource',
	'erreur_401_cle_nok_message' => 'Please use a valid access key to access the data in the "@extra@" collection.',
	'erreur_401_cle_nok_titre' => 'The "@valeur@" key is not authorized to access the collection',

	// F
	'form_vidage_index_explication' => 'Be careful not to delete the cache index below unless all response caches have been deleted.',

	// T
	'type_requete_collection_titre' => 'Collections',
	'type_requete_index_titre' => 'Collection index',
	'type_requete_ressource_titre' => 'Resources',
];
