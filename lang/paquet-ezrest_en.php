<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ezrest-paquet-xml-ezrest?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'ezrest_description' => 'This plugin is a tool for developing a REST API that conforms to the model imposed by the Abstract HTTP Server. 
_ It standardizes the parsing of requests and the construction of responses, so that the user plugin only has to worry about providing verification and data retrieval services.',
	'ezrest_slogan' => 'Simplify REST API implementation',
];
