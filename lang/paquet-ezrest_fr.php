<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezREST.git

return [

	// E
	'ezrest_description' => 'Ce plugin est un outil de développement d’une API REST conforme au modèle imposé par le Serveur HTTP abstrait. 
_ Il permet, de standardiser l’analyse des requêtes et la construction des réponses afin que le plugin utilisateur n’est plus qu’à se préoccuper de fournir les services de vérification et de récupération des données.',
	'ezrest_slogan' => 'Simplifier la mise en place d’une API REST',
];
