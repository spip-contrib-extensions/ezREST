<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezREST.git

return [

	// C
	'collection_filtre_facultatif' => 'facultatif',
	'collection_filtre_fournisseur' => 'ajouté par le plugin « @module@ »',
	'collection_filtre_obligatoire' => 'obligatoire',
	'collection_ressource_non' => 'lecture non autorisée',
	'collection_ressource_oui' => 'lecture autorisée, identifiant « @ressource@ »',

	// E
	'erreur_200_ok_message' => 'Les données collectées sont consultables à l’index « donnees ».',
	'erreur_200_ok_titre' => 'La requête a été traitée avec succès',
	'erreur_400_cle_obligatoire_nok_message' => 'Veuillez utiliser une clé d’accès « @valeur@ » valide.',
	'erreur_400_cle_obligatoire_nok_titre' => 'La clé d’accès « @valeur@ » est obligatoire',
	'erreur_400_cle_valeur_format_nok_message' => 'Veuillez utiliser pour la clé d’accès « @element@ » une valeur conforme au format « @extra@ ».',
	'erreur_400_cle_valeur_format_nok_titre' => 'La valeur « @valeur@ » de la clé d’accès « @element@ » n’est pas conforme',
	'erreur_400_cle_valeur_vide_nok_message' => 'Veuillez utiliser la clé d’accès « @valeur@ » avec une valeur valide non vide.',
	'erreur_400_cle_valeur_vide_nok_titre' => 'La clé d’accès « @valeur@ » ne peut pas être vide',
	'erreur_400_collection_indisponible_message' => 'L’API permet l’utilisation des collections suivantes : @extra@.',
	'erreur_400_collection_indisponible_titre' => 'La collection « @valeur@ » n’est pas fournie par l’API',
	'erreur_400_collection_nok_titre' => 'Problème avec la collection « @valeur@ »',
	'erreur_400_critere_nom_nok_message' => 'La collection  « @collection@ » supporte les paramètres suivants : @extra@.',
	'erreur_400_critere_nom_nok_titre' => 'Le paramètre « @valeur@ » n’est pas supporté par la collection « @collection@ »',
	'erreur_400_critere_obligatoire_nok_message' => 'Veuillez utiliser le paramètre « @valeur@ » avec une valeur autorisée.',
	'erreur_400_critere_obligatoire_nok_titre' => 'Le paramètre « @valeur@ » est obligatoire',
	'erreur_400_critere_valeur_format_nok_message' => 'Veuillez utiliser pour le paramètre « @element@ » une valeur conforme au format « @extra@ ».',
	'erreur_400_critere_valeur_format_nok_titre' => 'La valeur « @valeur@ » du paramètre « @element@ » n’est pas conforme',
	'erreur_400_critere_valeur_vide_nok_message' => 'Veuillez utiliser le paramètre « @valeur@ » avec une valeur autorisée non vide.',
	'erreur_400_critere_valeur_vide_nok_titre' => 'Le paramètre « @valeur@ » ne peut pas être vide',
	'erreur_400_ressource_indisponible_message' => 'L’API ne fournit des ressources que pour les collections suivantes : @extra@.',
	'erreur_400_ressource_indisponible_titre' => 'La collection « @collection@ » n’autorise pas l’accès à une ressource',
	'erreur_401_cle_nok_message' => 'Veuillez utiliser une clé d’accès valide pour être autorisé à accéder aux données de la collection « @extra@ ».',
	'erreur_401_cle_nok_titre' => 'La clé « @valeur@ » n’est pas autorisée à accéder à la collection',

	// F
	'form_vidage_index_explication' => 'Attention à ne supprimer l’index des caches ci-dessous que si tous les caches de type réponse ont bien été effacés.',

	// T
	'type_requete_collection_titre' => 'Collections',
	'type_requete_index_titre' => 'Index des collections',
	'type_requete_ressource_titre' => 'Ressources',
];
