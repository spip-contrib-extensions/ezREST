<?php
/**
 * Ce fichier contient l'ensemble des services de gestion des requêtes, des réponses et de traitement
 * des données des collections et des ressources.
 * Certains de ces services peuvent être personnalisés par l'appel d'un service spécifique du plugin utilisateur.
 *
 * @package SPIP\EZREST\SERVICE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// -----------------------------------------------------------------------
// ---------------- SERVICES DE GESTION GLOBALE DE L'API -----------------
// -----------------------------------------------------------------------

/**
 * Détermine si le serveur est capable de répondre aux requêtes.
 * Par défaut, l'API ezREST ne fait aucune vérification. C'est donc au plugin utilisateur de fournir
 * un service spécifique si une vérification globale doit être effectuée afin d'assurer le fonctionnement de
 * l'API.
 *
 * Si une erreur est détectée, le plugin utilisateur ne renvoie le type, l'élément et la valeur qui provoque l'erreur
 * sachant que c'est le service par défaut qui positionne le code.
 *
 * @uses service_ezrest_chercher()
 * @uses erreur_ezrest_initialiser()
 *
 * @param string               $plugin  Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param array<string, mixed> &$erreur Tableau initialisé avec les index identifiant l'erreur ou vide si pas d'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function ezrest_api_verifier_contexte(string $plugin, array &$erreur) : bool {
	// Initialise le retour à true par défaut.
	$est_valide = true;

	// Appel d'un service spéficique au plugin fournisseur pour vérifier si le contexte permet l'utilisation de l'API.
	if ($verifier = service_ezrest_chercher($plugin, 'api_verifier_contexte')) {
		// On initialise l'erreur avec son code 501 et son module, le plugin utilisateur.
		// Par contre, le type est passé à vide parce que c'est au plugin de choisir son identifiant d'erreur 501.
		$erreur = erreur_ezrest_initialiser($plugin, 501, '');
		$est_valide = $verifier($erreur);
	}

	// S'assurer que le bloc d'erreur est vide si aucune erreur n'a été détectée.
	if ($est_valide) {
		$erreur = [];
	}

	return $est_valide;
}

// -----------------------------------------------------------------------
// ------------------ SERVICES DE GESTION DES REPONSES -------------------
// -----------------------------------------------------------------------

/**
 * Initialise le contenu d'une réponse qui se présente comme un tableau associatif.
 * En particulier, la fonction stocke les éléments de la requête et positionne le bloc d'erreur
 * par défaut à ok.
 *
 * Ce service standard n'est pas personnalisable par un plugin utilisateur.
 *
 * @uses erreur_ezrest_initialiser()
 * @uses ezrest_reponse_expliquer_erreur()
 *
 * @param Request $requete Objet requête fourni par le plugin Serveur HTTP abstrait.
 *
 * @return array<string, mixed> Le contenu initial de la réponse est un tableau associatif à 3 entrées:
 *                              - `requete` : sous-tableau des éléments de la requête
 *                              - `erreur`  : sous-tableau des éléments descriptifs d'une erreur (status 200 par défaut)
 *                              - `donnees` : le tableau des objets demandés fonction de la requête (vide)
 */
function ezrest_reponse_initialiser_contenu(Request $requete) : array {
	// Stockage des éléments de la requête
	// -- La méthode
	$demande = ['methode' => $requete->getMethod()];
	// -- Les éléments format, collection et ressource
	$demande = array_merge($demande, $requete->attributes->all());
	// -- Les critères de filtre fournis comme paramètres de l'url.
	//    Si on utilise une URL classique avec spip.php il faut exclure certains paramètres.
	$demande['filtres'] = $requete->query->all();
	$demande['filtres'] = array_diff_key(
		$demande['filtres'],
		array_flip(['action', 'arg', 'lang', 'var_zajax'])
	);
	// -- Le format du contenu de la réponse est toujours le JSON
	$demande['format_contenu'] = 'json';

	// Initialisation du bloc d'erreur à ok par défaut
	$erreur = erreur_ezrest_initialiser('ezrest', 200, 'ok');
	$erreur = ezrest_reponse_expliquer_erreur('ezrest', $erreur);

	// On intitialise le contenu avec les informations collectées.
	// A noter que le format de sortie est initialisé par défaut à json indépendamment de la demande, ce qui permettra
	// en cas d'erreur sur le format demandé dans la requête de renvoyer une erreur dans un format lisible.
	return [
		'requete' => $demande,
		'erreur'  => $erreur,
		'donnees' => []
	];
}

/**
 * Complète l'initialisation du contenu d'une réponse avec des informations sur le plugin utilisateur.
 * REST Factory remplit de façon standard un nouvel index `plugin` du contenu et permet ensuite au plugin
 * utilisateur de personnaliser encore le contenu initialisé, si besoin.
 *
 * @uses service_ezrest_chercher()
 *
 * @param string               $plugin  Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param array<string, mixed> $contenu Le contenu de la réponse initialisé par `ezrest_reponse_initialiser_contenu`.
 *
 * @return array<string, mixed> Le contenu initial de la réponse complétée par l'index `fournisseur` qui fournit des informations
 *                              sur le plugin utilisateur.
 */
function ezrest_reponse_informer_plugin(string $plugin, array $contenu) : array {
	// On met à jour les informations sur le plugin utilisateur maintenant qu'il est connu.
	// -- A minima on enregistre le préfixe et la version du plugin.
	include_spip('inc/filtres');
	$informer = charger_filtre('info_plugin');
	$version = $informer($plugin, 'version', true);

	$contenu = array_merge(
		[
			'fournisseur' => [
				'plugin'  => strtoupper($plugin),
				'version' => $version,
			]
		],
		$contenu
	);

	// -- On ajoute le schéma de données du plugin si il existe.
	include_spip('inc/config');
	$schema = lire_config("{$plugin}_base_version");
	if (null !== $schema) {
		$contenu['fournisseur']['schema'] = $schema;
	}

	// Appel d'un service spécifique au plugin utilisateur pour compléter l'initialisation si besoin.
	if ($completer = service_ezrest_chercher($plugin, 'reponse_informer_plugin')) {
		$contenu = $completer($contenu);
	}

	return $contenu;
}

/**
 * Complète le bloc d'erreur avec le titre et l'explication de l'erreur.
 *
 * @uses service_ezrest_chercher()
 *
 * @param string               $plugin     Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param array<string, mixed> $erreur     Tableau initialisé avec les éléments de base de l'erreur
 *                                         (`status`, `type`, `element` et `valeur`).
 * @param null|string          $collection Identifiant de la collection
 *
 * @return array<string, mixed> Tableau de l'erreur complété avec le titre (index `title`) et le descriptif (index `detail`).
 */
function ezrest_reponse_expliquer_erreur(string $plugin, array $erreur, ?string $collection = '') : array {
	// Calcul des paramètres qui seront passés à la fonction de traduction.
	// -- on passe toujours la collection qui est vide uniquement pour l'erreur de serveur et l'extra qui peut aussi
	//    être vide parfois.
	$parametres = [
		'element'    => $erreur['element'],
		'valeur'     => $erreur['valeur'],
		'collection' => $collection,
		'extra'      => $erreur['extra']
	];
	// -- inutile de conserver l'extra dans le bloc d'erreur
	unset($erreur['extra']);

	// Traduction du libellé de l'erreur.
	$item = $erreur['module']['titre'] . ':erreur_' . $erreur['status'] . '_' . $erreur['type'] . '_titre';
	$erreur['titre'] = _T($item, $parametres);
	// Traduction du message complémentaire.
	$item = $erreur['module']['detail'] . ':erreur_' . $erreur['status'] . '_' . $erreur['type'] . '_message';
	$erreur['detail'] = _T($item, $parametres);
	// -- inutile de conserver l'information sur les modules fournissant les items de langue.
	unset($erreur['module']);

	// Appel d'un service spécifique au plugin utilisateur pour compléter le bloc d'erreur si besoin.
	if ($expliquer = service_ezrest_chercher($plugin, 'reponse_expliquer_erreur')) {
		$erreur = $expliquer($erreur);
	}

	return $erreur;
}

/**
 * Finalise la réponse à la requête en complétant le header et le contenu mis au préalable
 * au format JSON.
 *
 * Ce service standard n'est pas personnalisable par un plugin utilisateur.
 *
 * @param Response             $reponse Objet réponse tel qu'initialisé par le serveur HTTP abstrait.
 * @param array<string, mixed> $contenu Tableau du contenu de la réponse qui sera retourné en JSON.
 *
 * @return Response Retourne l'objet réponse dont le contenu et certains attributs du header sont mis à jour.
 */
function ezrest_reponse_construire(Response $reponse, array $contenu) : Response {
	// Charset UTF-8 et statut de l'erreur.
	$reponse->setCharset('utf-8');
	$reponse->setStatusCode($contenu['erreur']['status']);

	// Format JSON exclusif pour les réponses.
	$reponse->headers->set('Content-Type', 'application/json');
	$reponse->setContent(json_encode($contenu));

	return $reponse;
}

// -----------------------------------------------------------------------
// ------------------ SERVICES DE GESTION DES REQUETES -------------------
// -----------------------------------------------------------------------

/**
 * Détermine si la collection demandée est valide. Par défaut, REST Factory vérifie que la collection est bien
 * déclarée dans la liste des collections. Si c'est le cas, la fonction permet ensuite au plugin utilisateur de
 * compléter la vérification, si besoin.
 *
 * @uses erreur_ezrest_initialiser()
 * @uses service_ezrest_chercher()
 *
 * @param string               $collection L'identifiant de la collection demandée
 * @param string               &$plugin    Préfixe du plugin fournisseur de la collection. Chaine vide en entrée et en sortie si
 *                                         une erreur est détectée.
 * @param array<string, mixed> &$erreur
 *                                         Tableau initialisé avec les index identifiant l'erreur ou vide si pas d'erreur.
 *                                         Les index mis à jour sont:
 *                                         - `status`  : le code de l'erreur HTTP, soit 400
 *                                         - `type`    : chaine identifiant l'erreur plus précisément, soit collection_nok
 *                                         - `element` : type d'objet sur lequel porte l'erreur, soit collection
 *                                         - `valeur`  : la valeur de la collection
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function ezrest_collection_verifier(string $collection, string &$plugin, array &$erreur) : bool {
	// Initialise le retour à false par défaut.
	$est_valide = false;

	// Récupération de la liste des collections disponibles.
	$declarer = charger_fonction('ezrest_declarer_collections', 'inc');
	$collections = $declarer();

	// Vérification de la disponibilité de la collection demandée.
	foreach ($collections as $_collection => $_configuration) {
		if ($collection == $_collection) {
			// la collection est déclarée, on renvoie le plugin fournisseur et aucune erreur.
			$est_valide = true;
			$plugin = $_configuration['module'];
			break;
		}
	}

	// La collection n'est pas déclarée, on renvoie une erreur et pas de plugin.
	if (!$est_valide) {
		$erreur = erreur_ezrest_initialiser(
			'ezrest',
			400,
			'collection_indisponible',
			'collection',
			$collection,
			implode(', ', array_keys($collections))
		);
	} elseif ($verifier = service_ezrest_chercher($plugin, 'verifier', $collection)) {
		// Appel d'un service spécifique au plugin utilisateur pour compléter la vérification si besoin.
		$erreur = erreur_ezrest_initialiser(
			$plugin,
			400,
			'collection_nok',
			'collection',
			$collection
		);
		$est_valide = $verifier($erreur);
	}

	// S'assurer que le bloc d'erreur est vide si aucune erreur n'a été détectée.
	if ($est_valide) {
		$erreur = [];
	}

	return $est_valide;
}

/**
 * Détermine si la valeur de chaque critère de filtre d'une collection est valide.
 * Si plusieurs critères sont fournis, la fonction s'interromp dès qu'elle trouve un
 * critère non admis ou dont la valeur est invalide.
 *
 * @uses erreur_ezrest_initialiser()
 * @uses service_ezrest_chercher()
 *
 * @param string               $plugin        Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param array<string, mixed> $filtres       Tableau associatif des critères de filtre (couple nom du critère, valeur du critère)
 * @param string               $collection    L'identifiant de la collection concernée.
 * @param array<string, mixed> $configuration Configuration de la collection concernée. L'index `filtres` contient la liste des
 *                                            critères admissibles et l'index `module` contient le nom du fichier des fonctions de service.
 * @param array<string, mixed> &$erreur       Tableau initialisé avec les index identifiant l'erreur ou vide si pas d'erreur.
 *                                            Les index mis à jour sont:
 *                                            - `status`  : le code de l'erreur HTTP, soit 400
 *                                            - `type`    : chaine identifiant l'erreur plus précisément
 *                                            - `element` : nom du critère en erreur
 *                                            - `valeur`  : valeur du critère
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function ezrest_collection_verifier_filtre(string $plugin, array $filtres, string $collection, array $configuration, array &$erreur) : bool {
	// Initialise le retour à true par défaut.
	$est_valide = true;

	if ($filtres) {
		foreach ($configuration['filtres'] as $_filtre) {
			if (
				!empty($_filtre['est_obligatoire'])
				and (!isset($filtres[$_filtre['critere']]))
			) {
				// 1- Absence de critère obligatoire.
				$type = !empty($_filtre['est_cle']) ? 'cle_obligatoire_nok' : 'critere_obligatoire_nok';
				$erreur = erreur_ezrest_initialiser(
					'ezrest',
					400,
					$type,
					'critere',
					$_filtre['critere']
				);
				$est_valide = false;
				break;
			}
		}

		if ($est_valide) {
			foreach ($filtres as $_critere => $_valeur) {
				$criteres = array_column($configuration['filtres'], null, 'critere');
				$prefixe_type = !empty($criteres[$_critere]['est_cle']) ? 'cle' : 'critere';
				if (!in_array($_critere, array_keys($criteres))) {
					// 2- Paramètre non autorisé.
					$erreur = erreur_ezrest_initialiser(
						'ezrest',
						400,
						'critere_nom_nok',
						'critere',
						$_critere,
						implode(', ', array_keys($criteres))
					);
					$est_valide = false;
					break;
				} elseif (
					!empty($criteres[$_critere]['vide_interdit'])
					and (empty($_valeur))
				) {
					// 3- Paramètre vide non autorisé.
					$erreur = erreur_ezrest_initialiser(
						'ezrest',
						400,
						"{$prefixe_type}_valeur_vide_nok",
						'critere',
						$_critere
					);
					$est_valide = false;
					break;
				} elseif (
					!empty($criteres[$_critere]['format'])
					and !preg_match($criteres[$_critere]['format'], $_valeur)
				) {
					// 4- Paramètre non conforme à une regex
					$erreur = erreur_ezrest_initialiser(
						'ezrest',
						400,
						"{$prefixe_type}_valeur_format_nok",
						$_critere,
						$_valeur,
						$criteres[$_critere]['format']
					);
					$est_valide = false;
					break;
				} else {
					// 5- le critère est vérifié par une fonction spécifique qui est soit liée au critère soit globale à
					//    la fonction. Si cette fonction n'existe pas le critère est réputé valide.
					//    On distingue aussi le critère clé d'accès qui appelle une fonction de vérification spécifique
					//    dont le nom est toujours le même (indépendant du nom du paramètre).
					$module = !empty($criteres[$_critere]['module'])
						? $criteres[$_critere]['module']
						: $plugin;
					if (empty($criteres[$_critere]['est_cle'])) {
						$fonction = 'verifier_filtre';
						$suffixe = $_critere;
						$erreur = erreur_ezrest_initialiser(
							$module,
							400,
							'',
							$_critere,
							$_valeur
						);
					} else {
						$fonction = 'verifier';
						$suffixe = $prefixe_type;
						$erreur = erreur_ezrest_initialiser(
							$module,
							401,
							"{$prefixe_type}_nok",
							$_critere,
							$_valeur,
							$collection
						);
					}
					if ($verifier = service_ezrest_chercher($module, $fonction, $collection, $suffixe)) {
						if (!$verifier($_valeur, $erreur)) {
							$est_valide = false;
							break;
						}
					}
				}
			}
		}
	}

	// S'assurer que le bloc d'erreur est vide si aucune erreur n'a été détectée.
	if ($est_valide) {
		$erreur = [];
	}

	return $est_valide;
}

/**
 * Détermine si le type de ressource demandée est valide.
 *
 * @uses erreur_ezrest_initialiser()
 * @uses service_ezrest_chercher()
 *
 * @param string               $plugin        Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param string               $ressource     La valeur de la ressource demandée. La ressource appartient à une collection.
 *                                            On considère que la ressource est passée comme une chaine, c'est au plugin utilisateur de connaitre son type réel.
 * @param string               $collection    L'identifiant de la collection concernée.
 * @param array<string, mixed> $configuration Configuration de la collection de la ressource. L'index `ressource` identifie le champ
 *                                            attendu pour désigner la ressource et l'index `module` contient le nom du fichier des
 *                                            fonctions de service.
 * @param array<string, mixed> &$erreur       Tableau initialisé avec les index identifiant l'erreur ou vide si pas d'erreur.
 *                                            Les index mis à jour sont:
 *                                            - `status`  : le code de l'erreur HTTP, soit 400
 *                                            - `type`    : chaine identifiant l'erreur plus précisément, soit ressource_nok
 *                                            - `element` : type d'objet sur lequel porte l'erreur, soit ressource
 *                                            - `valeur`  : la valeur de la ressource
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function ezrest_collection_verifier_ressource(string $plugin, string $ressource, string $collection, array $configuration, array &$erreur) : bool {
	// Initialise le retour à true par défaut.
	$est_valide = true;

	// Vérification de la disponibilité de l'accès à une ressource pour la collection concernée
	if (empty($configuration['ressource'])) {
		// Récupération de la liste des collections disponibles pour lister celles avec ressources dans le message.
		$declarer = charger_fonction('ezrest_declarer_collections', 'inc');
		$collections = $declarer();
		$ressources = [];
		foreach ($collections as $_collection => $_configuration) {
			if (!empty($_configuration['ressource'])) {
				$ressources[] = $_collection;
			}
		}
		$erreur = erreur_ezrest_initialiser(
			'ezrest',
			400,
			'ressource_indisponible',
			'ressource',
			$ressource,
			implode(', ', $ressources)
		);
		$est_valide = false;
	} elseif ($verifier = service_ezrest_chercher($plugin, 'verifier_ressource', $collection, $configuration['ressource'])) {
		// Vérification de la validité de la ressource demandée.
		// -- la ressource est vérifiée par une fonction spécifique. Si elle n'existe pas la ressource est
		//    réputée valide.
		$erreur = erreur_ezrest_initialiser(
			$plugin,
			400,
			'',
			'ressource',
			$ressource,
			$configuration['ressource']
		);
		if (!$verifier($ressource, $erreur)) {
			$est_valide = false;
		}
	}

	// S'assurer que le bloc d'erreur est vide si aucune erreur n'a été détectée.
	if ($est_valide) {
		$erreur = [];
	}

	return $est_valide;
}

// -----------------------------------------------------------------------
// ------------------ SERVICES DE GESTION DES DONNEES --------------------
// -----------------------------------------------------------------------

/**
 * Construit l'index des collections disponibles avec leur configuration expliquée.
 *
 * @return array<string, mixed> Tableau des configurations des collections disponibles classées par plugin fournisseur.
 */
function ezrest_indexer() : array {
	// Initialisation des données de la collection à retourner
	$contenu = [];

	// Récupération de la liste des collections disponibles.
	$declarer = charger_fonction('ezrest_declarer_collections', 'inc');
	$collections = $declarer();

	// On contruit la liste des collections disponibles en présentant leur configuration d'une façon la plus
	// explicite pour les utilisateurs. Les collections sont présentées sous chaque plugin fournisseur.
	foreach ($collections as $_collection => $_configuration) {
		// Détermination du plugin fournisseur
		$plugin = $_configuration['module'];

		// Si c'est la première fois qu'on rencontre le plugin alors on stocke quelques informations sur le plugin.
		if (!isset($contenu[$plugin])) {
			include_spip('inc/filtres');
			$informer = charger_filtre('info_plugin');
			$version = $informer($plugin, 'version', true);
			$nom = $informer($plugin, 'nom', true);

			$contenu[$plugin]['fournisseur'] = [
				'nom'     => extraire_multi($nom),
				'version' => $version,
			];
		}

		// On initialise le contenu de la collection sous l'index du plugin
		$contenu[$plugin]['collections'][$_collection] = [];

		// -- Informer sur la possibilité de demander une ressource
		$contenu[$plugin]['collections'][$_collection]['ressource'] = isset($_configuration['ressource'])
			? _T('ezrest:collection_ressource_oui', ['ressource' => $_configuration['ressource']])
			: _T('ezrest:collection_ressource_non');

		// -- Informer sur les filtres autorisés
		if ($_configuration['filtres']) {
			foreach ($_configuration['filtres'] as $_filtre) {
				$obligatoire = empty($_filtre['est_obligatoire'])
					? _T('ezrest:collection_filtre_facultatif')
					: _T('ezrest:collection_filtre_obligatoire');
				$fournisseur = empty($_filtre['module'])
					? ''
					: ', ' . _T('ezrest:collection_filtre_fournisseur', ['module' => strtoupper($_filtre['module'])]);
				$contenu[$plugin]['collections'][$_collection]['filtres'][$_filtre['critere']] = $obligatoire . $fournisseur;
			}
		}
	}

	return $contenu;
}

/**
 * Construit le contexte à fournir au fichier HTML qui produira le JSON des données de la réponse.
 * Cette fonction est utilisée si la méthode de cache choisie dans la configuration de la collection est `spip`.
 *
 * @param string               $plugin        Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param string               $collection    L'identifiant de la collection concernée.
 * @param array<string, mixed> $parametres    Tableau associatif des paramètres possiblement utiles au contexte.
 *                                            Cet argument est principalement utilisé pour fournir les filtres.
 *                                            Pour une ressource, l'identifiant et la valeur de la ressource sont aussi fournis
 *                                            à l'instar des éventuels filtres.
 * @param array<string, mixed> $configuration Configuration de la collection concernée.
 *
 * @return array<string, mixed> Le tableau de contexte à fournir au fichier HTML.
 */
function ezrest_contextualiser(string $plugin, string $collection, array $parametres, array $configuration) {
	// Initialisation minimale du contexte : le préfixe du plugin est passé sous le terme plugin_prefixe
	// pour éviter une collision avec des balises #PREFIXE ou #PLUGIN.
	$contexte = [
		'plugin_prefixe' => $plugin,
		'collection'     => $collection,
		'configuration'  => $configuration
	];

	// Ajout des paramètres au contexte : on ne retient que ceux qui ont une influence sur le contenu.
	if ($parametres) {
		// Extraire la configuration des critères pour construire le contexte induit par les filtres
		$criteres = array_column($configuration['filtres'], null, 'critere');

		// On boucle sur les paramètres fournis qui sont :
		// - les filtres, que la requête concerne une collection ou une ressource
		// - le couple (champ de ressource, valeur ressource) si la requête concerne une ressource
		foreach ($parametres as $_parametre => $_valeur) {
			// On exclut les filtres qui n'ont aucune influence sur le contenu de la réponse.
			// -- La ressource éventuelle passée dans les paramètres sera automatiquement prise en compte car elle
			//    ne possède pas de configuration comme un filtre.
			if (empty($criteres[$_parametre]['hors_contenu'])) {
				$nom_champ = !empty($criteres[$_parametre]['champ_nom'])
					? $criteres[$_parametre]['champ_nom']
					: $_parametre;
				$contexte[$nom_champ] = $_valeur;
			}
		}
	}

	return $contexte;
}

/**
 * Construit l'identifiant relatif du fichier cache (plugin Cache Factory) dans lequel sera stockée la réponse
 * à la requête.
 *
 * Le préfixe du plugin sert à définir le sous-dossier de stockage.
 * Le type de requête (`collection`, `ressource`, `index`), l'identifiant de la collection et un éventuellement complément
 * forment les composants du nom du fichier cache.
 *
 * @param string                    $plugin        Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param string                    $type_requete  Indique si on demande l'index, une collection ou une ressource.
 * @param null|string               $collection    L'identifiant de la collection concernée.
 * @param null|array<string, mixed> $parametres    Tableau associatif des paramètres complémentaires pour calculer l'identifiant.
 *                                                 Cet argument est principalement utilisé pour compiler les filtres.
 *                                                 Pour une ressource, l'identifiant et la valeur de la ressource sont aussi fournis
 *                                                 à l'instar des éventuels filtres.
 * @param null|array<string, mixed> $configuration Configuration de la collection concernée.
 *
 * @return array<string, mixed> Tableau de l'identifiant relatif du fichier cache.
 */
function ezrest_cache_identifier(string $plugin, string $type_requete, ?string $collection = 'collections', ?array $parametres = [], ?array $configuration = []) : array {
	// Initialisation du tableau d'identification du cache
	$cache = [];

	// Mise à jour du sous-dossier en fonction du plugin et du type de requete
	$cache['sous_dossier'] = $plugin;

	// Elements du nom du cache
	// -- le type de requête est toujours le préfixe du nom et la collection est mise à 'collections' si le
	//    type de requête est l'index.
	$cache['type_requete'] = $type_requete;
	$cache['collection'] = $collection;

	// -- Si le cache n'est pas celui de l'index on complète le nom avec le tableau complément.
	//    On hash toujours la ressource et les filtres de façon à ne jamais avoir un problème de nom de fichier.
	//    De fait, on loge aussi toujours les éléments du complément.
	if (
		($type_requete != 'index')
		and $parametres
	) {
		// Récupération de la configuration des filtres
		$criteres = array_column($configuration['filtres'], null, 'critere');

		// On boucle sur les paramètres fournis qui sont :
		// - les filtres, que la requête concerne une collection ou une ressource
		// - le couple (champ de ressource, valeur ressource) si la requête concerne une ressource
		$hash_texte = '';
		$hash_tableau = [];
		foreach ($parametres as $_parametre => $_valeur) {
			// On exclut les filtres qui n'ont aucune influence sur le contenu de la réponse.
			// -- La ressource éventuelle passée dans les paramètres sera automatiquement prise en compte car elle
			//    ne possède pas de configuration comme un filtre.
			if (empty($criteres[$_parametre]['hors_contenu'])) {
				$hash_texte .= "{$_parametre}{$_valeur}";
				$hash_tableau[$_parametre] = $_valeur;
			}
		}
		// On ajoute le composant complément et le composant filtres source de façon à pouvoir les loger
		// dans l'index des caches.
		$cache['complement'] = md5($hash_texte);
		$cache['filtres'] = $hash_tableau;
	}

	// Durée de conservation du cache : si précisée pour la collection on l'utilise sinon on utilise la valeur
	// par défaut des caches REST Factory (1 jour).
	if (isset($configuration['cache']['duree'])) {
		$cache['conservation'] = $configuration['cache']['duree'];
	}

	return $cache;
}

/**
 * Construit le tableau des conditions SQL à appliquer à la collection.
 *
 * @uses service_ezrest_chercher()
 *
 * @param string               $plugin        Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param string               $collection    L'identifiant de la collection concernée.
 * @param array<string, mixed> $filtres       Tableau associatif des critères de filtre (couple nom du critère, valeur du critère)
 * @param array<string, mixed> $configuration Configuration de la collection concernée.
 *
 * @return array Tableau des conditions SQL
 */
function ezrest_conditionner(string $plugin, string $collection, array $filtres, array $configuration) : array {
	// Initialisation des données de la collection à retourner
	$conditions = [];

	// Détermination de la fonction de service permettant de récupérer la collection spécifiée
	// filtrée sur les critères éventuellement fournis.
	if (
		empty($configuration['sans_condition'])
		and $filtres
	) {
		include_spip('base/objets');
		// Extraire la configuration des critères
		$criteres = array_column($configuration['filtres'], null, 'critere');
		foreach ($filtres as $_critere => $_valeur) {
			// On génère une condition uniquement si le critère ne spécifie pas le contraire
			if (empty($criteres[$_critere]['sans_condition'])) {
				// On regarde si il y une fonction particulière permettant le calcul du critère ou si celui-ci
				// est calculé de façon standard.
				$module = !empty($criteres[$_critere]['module'])
					? $criteres[$_critere]['module']
					: $plugin;
				if ($conditionner = service_ezrest_chercher($module, 'conditionner', $collection, $_critere)) {
					// La condition est élaborée par une fonction spécifique du plugin utilisateur.
					// Il est donc inutile de fournir autre chose que la valeur à la fonction spécifique car tout le
					// contexte est déjà connu du plugin utilisateur.
					if ($condition = $conditionner($_valeur)) {
						$conditions[] = $condition;
					}
				} else {
					// La condition est calculée par REST Factory à partir de la configuration du filtre.
					// -- détermination du nom du champ servant de critère
					$nom_champ = !empty($criteres[$_critere]['champ_nom'])
						? $criteres[$_critere]['champ_nom']
						: $_critere;

					// -- détermination de la table à ajouter en préfixe du champ :
					//    - si l'index 'champ_table' n'est pas précisé on utilise le nom de la collection : si elle
					//      correspond à une table on l'utilise en préfixe sinon on ne préfixe pas.
					//    - si l'index 'champ_table' est précisé : si il est vide, on ne préfixe pas, sinon on l'utilise
					//      pour trouver la table et l'utiliser en préfixe.
					$type_objet = !isset($criteres[$_critere]['champ_table'])
						? $collection
						: $criteres[$_critere]['champ_table'];
					$table = table_objet_sql($type_objet);
					$champ_sql = ($type_objet and ($table != $type_objet))
						? "{$table}.{$nom_champ}"
						: $nom_champ;

					// -- détermination de la fonction à appliquer à la valeur en fonction de son type (défaut string).
					$fonction = (empty($criteres[$_critere]['champ_type']) or ($criteres[$_critere]['champ_type'] == 'string'))
						? 'sql_quote'
						: 'intval';

					// Construction de la condition et ajout en queue du tableau.
					$conditions[] = "{$champ_sql}=" . $fonction($_valeur);
				}
			}
		}
	}

	return $conditions;
}

/**
 * Renvoie les données correspondant à la collection et aux filtres demandés.
 *
 * @uses service_ezrest_chercher()
 *
 * @param string               $plugin        Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param string               $collection    L'identifiant de la collection concernée.
 * @param array                $conditions    Tableau des conditions SQL à appliquer, compilées à partir des filtres de la requête.
 * @param array<string, mixed> $filtres       Tableau associatif des critères de filtre (couple nom du critère, valeur du critère)
 * @param array<string, mixed> $configuration Configuration de la collection concernée.
 *
 * @return array Données correspondant à la requête sur la collection
 */
function ezrest_collectionner(string $plugin, string $collection, array $conditions, array $filtres, array $configuration) : array {
	// Initialisation des données de la collection à retourner
	$contenu = [];

	// Détermination de la fonction de service permettant de récupérer la collection spécifiée
	// filtrée sur les critères éventuellement fournis.
	if ($collectionner = service_ezrest_chercher($plugin, 'collectionner', $collection)) {
		$contenu = $collectionner($conditions, $filtres, $configuration);
	}

	return $contenu;
}

/**
 * Renvoie les données correspondant à la ressource demandée.
 *
 * @uses service_ezrest_chercher()
 *
 * @param string               $plugin        Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param string               $collection    L'identifiant de la collection concernée.
 * @param string               $ressource     La valeur de la ressource demandée. La ressource appartient à une collection.
 *                                            On considère que la ressource est passée comme une chaine, c'est au plugin utilisateur de connaitre son type réel.
 * @param array<string, mixed> $filtres       Tableau associatif des critères de filtre (couple nom du critère, valeur du critère)
 * @param array<string, mixed> $configuration Configuration de la collection concernée.
 *
 * @return array Données correspondant à la requête sur la ressource
 */
function ezrest_ressourcer(string $plugin, string $collection, string $ressource, array $filtres, array $configuration) {
	// Initialisation des données de la collection à retourner
	$contenu = [];

	// Détermination de la fonction de service permettant de récupérer la collection spécifiée
	// filtrée sur les critères éventuellement fournis.
	if ($ressourcer = service_ezrest_chercher($plugin, 'ressourcer', $collection)) {
		$contenu = $ressourcer($ressource, $filtres, $configuration);
	}

	return $contenu;
}

// -----------------------------------------------------------------------
// ------------------- UTILITAIRES PROPRE AU PLUGIN ----------------------
// -----------------------------------------------------------------------

/**
 * Cherche une fonction donnée en se basant sur le plugin appelant.
 * Si le plugin utilisateur ne fournit pas la fonction demandée la chaîne vide est renvoyée.
 *
 * @internal
 *
 * @param string      $plugin   Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 *                              Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                              un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string      $fonction Nom de la fonction de service à chercher.
 * @param null|string $prefixe  Préfixe à accoler au nom de la fonction (incompatible avec le préfixe du plugin).
 *                              Dans tous les cas actuels correspond au nom de la collection.
 * @param null|string $suffixe  Suffixe à accoler au nom de la fonction (incompatible avec le préfixe du plugin).
 *                              Correspond au nom d'un critère ou d'une ressource.
 *
 * @return string Nom complet de la fonction si trouvée ou chaine vide sinon.
 */
function service_ezrest_chercher(string $plugin, string $fonction, ?string $prefixe = '', ?string $suffixe = '') {
	$fonction_trouvee = '';

	// Eviter la réentrance si on demande explicitement le plugin ezrest.
	if ($plugin != 'ezrest') {
		include_spip("ezrest/{$plugin}");
		$fonctions_possibles = [];
		if (!$prefixe) {
			$fonctions_possibles[] = "{$plugin}_{$fonction}";
		} elseif (!$suffixe) {
			$fonctions_possibles[] = "{$prefixe}_{$fonction}";
		} else {
			$fonctions_possibles[] = "{$prefixe}_{$fonction}_{$suffixe}";
			$fonctions_possibles[] = "{$plugin}_{$fonction}_{$suffixe}";
		}

		foreach ($fonctions_possibles as $_fonction) {
			if (function_exists($_fonction)) {
				$fonction_trouvee = $_fonction;
				break;
			}
		}
	}

	return $fonction_trouvee;
}

/**
 * Initialise le bloc d'erreur complet.
 *
 * @internal
 *
 * @param string      $plugin  Préfixe du plugin utilisateur de ezrest.
 * @param int         $code    Code d'erreur standard d'une requête HTTP
 * @param string      $type    Identifiant unique de l'erreur
 * @param null|string $element Elément sur lequel porte l'erreur
 * @param null|string $valeur  Valeur de l'élément en erreur
 * @param null|string $extra   Complément d'information
 *
 * @return array<string, mixed> Bloc d'erreur complet initialisé avec le code, le plugin et le type.
 */
function erreur_ezrest_initialiser(string $plugin, int $code, string $type, ?string $element = '', ?string $valeur = '', ?string $extra = '') {
	// On initialise tous les index d'un bloc d'erreur
	$erreur['status'] = $code;
	$erreur['type'] = $type;
	$erreur['element'] = $element;
	$erreur['valeur'] = $valeur;
	$erreur['extra'] = $extra;
	$erreur['titre'] = '';
	$erreur['detail'] = '';

	// Si le type est précisé c'est que son item de langue est fourni par ezREST.
	// Sinon, c'est que son item de langue est fourni par le plugin passé en paramètre.
	$erreur['module']['titre'] = $type ? 'ezrest' : $plugin;
	$erreur['module']['detail'] = $type ? 'ezrest' : $plugin;

	return $erreur;
}

/**
 * Renvoie la configuration d'une collection.
 * Si la collection possède une clé d'accès, la fonction ajoute le filtre idoine dans la configuration de la collection.
 *
 * @internal
 *
 * @param string $collection Nom de la collection.
 *
 * @return array<string, mixed> Tableau de la collection complètement initialisé.
 */
function configuration_ezrest_lire(string $collection) {
	static $configurations = [];

	if (!isset($configurations[$collection])) {
		// Initialisation en cas d'erreur de collection
		$configurations[$collection] = [];

		// Récupération de la liste des collections disponibles.
		$declarer = charger_fonction('ezrest_declarer_collections', 'inc');
		$collections = $declarer();

		if (isset($collections[$collection])) {
			// On charge la configuration brute de la collection
			$configurations[$collection] = $collections[$collection];

			// Si l'accès à la collection nécessite une clé, on construit une configuration idoine afin que la clé
			// soit traitée comme un filtre.
			if (!empty($configurations[$collection]['cle']['critere'])) {
				// On considère la clé d'accès comme un filtre obligatoire, dont la valeur n'est jamais vide, qui
				// ne génère pas de condition SQL et n'influe pas sur le contenu de la réponse.
				$filtre_cle = [
					'critere'         => $configurations[$collection]['cle']['critere'],
					'est_obligatoire' => true,
					'vide_interdit'   => true,
					'sans_condition'  => true,
					'hors_contenu'    => true,
					'format'          => $configurations[$collection]['cle']['format'] ?? '',
					'est_cle'         => true,
				];
				$configurations[$collection]['filtres'] = array_merge(
					[$filtre_cle],
					$configurations[$collection]['filtres'] ?? []
				);
			}
		}
	}

	return $configurations[$collection];
}
