<?php
/**
 * Ce fichier contient les cas d'utilisation de certains pipelines par le plugin REST Factory.
 *
 * @package SPIP\EZREST\PIPELINE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajoute ou retire le cache créé ou supprimé de l'index des caches REST Factory.
 * Les informations contenues permettent l'affichage précis dans le formulaire de vidage.
 *
 * @uses cache_lire()
 * @uses cache_ecrire()
 *
 * @pipeline post_cache
 *
 * @param array $flux Tableau des données permettant de caractériser l'appelant et l'action sur les caches.
 *
 * @return array Le flux entrant n'est pas modifié.
 *
 * @throws Exception
 */
function ezrest_post_cache(array $flux) : array {
	// On vérifie que l'appel du pipeline est bien consécutif à une action sur les caches de REST Factory
	if (
		($flux['args']['plugin'] === 'ezrest')
		and ($flux['args']['configuration']['type_cache'] !== 'index')
	) {
		// Lecture du fichier d'index et récupération du tableau des caches enregistrés.
		include_spip('inc/ezcache_cache');
		$cache_index = [];
		$index = cache_lire('ezrest', 'index', $cache_index);

		// Extraction du fichier cache : on utilise juste le nom et le répertoire du plugin ce qui suffit pour être unique.
		$fichier_cache = basename(dirname($flux['args']['fichier_cache'])) . '/' . basename($flux['args']['fichier_cache']);

		if ($flux['args']['fonction'] == 'ecrire') {
			// On vient d'écrire un cache, on le loge dans l'index.
			$index[$fichier_cache] = $flux['args']['cache'];
		} elseif ($flux['args']['fonction'] == 'supprimer') {
			// On vient de supprimer un cache, on le retire de l'index.
			if ($index and isset($index[$fichier_cache])) {
				unset($index[$fichier_cache]);
			}
		}

		// Mise à jour de l'index sans appeler le pipeline post_cache pour éviter la réentrance.
		cache_ecrire('ezrest', 'index', $cache_index, $index, false);
	}

	return $flux;
}
