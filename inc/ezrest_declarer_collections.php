<?php
/**
 * Ce fichier contient la fonction de déclaration des configurations de collections de l'api `ezrest`.
 * Elle appelle le pipeline `liste_ezcollection` pour les plugins qui le souhaitent.
 *
 * @package SPIP\EZREST\COLLECTION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclare les collections accessibles via HTTP GET.
 * Par défaut, le plugin ne propose aucune collection.
 *
 * @pipeline_appel liste_ezcollection
 *
 * @return array Description des collections.
**/
function inc_ezrest_declarer_collections_dist() : array {
	// Initialisation en static pour les performances du tableau de toutes les collections
	static $collections = [];

	if (empty($collections)) {
		// Le plugin REST Factory ne fournit aucune collection par défaut. Il convient à chaque plugin utilisateur
		// de fournir ses configurations.
		$collections = pipeline('liste_ezcollection', $collections);
	}

	return $collections;
}
