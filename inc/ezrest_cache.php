<?php
/**
 * Ce fichier contient les fonctions d'API du plugin REST Factory.
 *
 * @package SPIP\EZREST\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vide, pour un plugin utilisateur donné, les caches du type `reponse` éventuellement filtrés.
 *
 * @api
 *
 * @uses cache_repertorier()
 * @uses cache_vider()
 *
 * @param string      $plugin       Préfixe du plugin utilisateur de ezrest et donc fournisseur de la collection.
 * @param null|string $type_requete Indique si on demande l'index, une collection ou une ressource.
 * @param null|string $collection   L'identifiant de la collection concernée.
 *
 * @return bool `true` si la suppression s'est bien passée, `false` sinon.
 */
function cache_reponse_vider(string $plugin, ?string $type_requete = '', ?string $collection = '') : bool {
	// Initialisation du retour de la fonction
	$cache_vide = false;

	// On récupère des caches
	include_spip('inc/ezcache_cache');
	$type_cache = 'reponse';
	$filtres = [
		'sous_dossier' => $plugin
	];
	if ($type_requete) {
		$filtres['type_requete'] = $type_requete;
	}
	if ($collection) {
		$filtres['collection'] = $collection;
	}
	$caches = cache_repertorier('ezrest', $type_cache, $filtres);

	// On supprimer les caches récupérés
	if ($caches) {
		cache_vider('ezrest', $type_cache, array_keys($caches));
		$cache_vide = true;
	}

	return $cache_vide;
}
